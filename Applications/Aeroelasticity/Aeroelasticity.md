# Aeroelasticity

## Introduction
In progress...

[comment]: # (########################################################)
## The Vortex Lattice Method (VLM)
![alt text](./Figures/1-VLM-FlatPlateWing.jpeg)

### Validation

[comment]: # (--------------------------------------------------------)
#### Test \#1: Comparison with lifting-line theory solution
We consider a wing with a rectangular planform of chord $c = 1$ m and span $b = 20c$; the wing has zero dihedral angle and zero sweep angle, i.e., $\Gamma = 0$ and $\Lambda = 0$, respectively.
The wing is subjected to a uniform free-stream velocity $\boldsymbol{V}_\infty = 10$ m/s at an angle of attack $\alpha = 1^\circ$.

The wing is discretized as ...

![alt text](./Figures/1-VLM-FlatPlateWing-EffectOfMesh-a.jpeg) ![alt text](./Figures/1-VLM-FlatPlateWing-EffectOfMesh-b.jpeg)

The left figure above shows ...

[comment]: # (--------------------------------------------------------)

#### Test \#2: Comparison with 2D flat-plate airfoil solution
We consider the same wing as in [Test \#1](https://gitlab.com/aeropa/pysco/-/blob/main/Applications/Aeroelasticity/Aeroelasticity.md?ref_type=heads#test-1-comparison-with-lifting-line-theory-solution) subjected to a uniform free-stream velocity $\boldsymbol{V}_\infty = 10$ m/s at an angle of attack $\alpha = 10^\circ$.

![alt text](./Figures/1-VLM-FlatPlateWing-EffectOfMesh-c.jpeg) ![alt text](./Figures/1-VLM-FlatPlateWing-EffectOfMesh-d.jpeg)

[comment]: # (--------------------------------------------------------)

[comment]: # (--------------------------------------------------------)
#### Test \#3: Effect of sweep angle

![alt text](./Figures/1-VLM-FlatPlateWing-EffectOfSweepAngle-a.jpeg)

The comparison in terms of  $c_{L_\alpha}$ is reported in the table below
|  $\Lambda$    |  Present          |  Ref.[1]         |
| :-----------: | :---------------: | :--------------: |
|  0$^\circ$    |  3.64             |  3.63            |
|  45$^\circ$   |  3.02             |  3.00            |
|  -45$^\circ$  |  3.01             |  2.99            |

[comment]: # (--------------------------------------------------------)

[comment]: # (--------------------------------------------------------)
#### Test \#4: Effect of aspect ratio and sweep angle

![alt text](./Figures/1-VLM-FlatPlateWing-EffectOfAspectRatioAndSweepAngle-a.jpeg)

[comment]: # (--------------------------------------------------------)

[comment]: # (--------------------------------------------------------)
#### Test \#5: Effect of taper ratio

![alt text](./Figures/1-VLM-FlatPlateWing-EffectOfTaperRatio-a.jpeg)

[comment]: # (--------------------------------------------------------)

[comment]: # (--------------------------------------------------------)
#### Test \#6: Ground effect

[comment]: # (--------------------------------------------------------)
[comment]: # (########################################################)



[comment]: # (########################################################)
## The Unsteady Vortex Lattice Method (UVLM)

### Validation

#### Test \#1: Wake shed by a flat-plate wing

![alt text](./Figures/2-UVLM-FlatPlateWing-EffectOfAspectRatio-a.jpeg) ![alt text](./Figures/2-UVLM-FlatPlateWing-EffectOfAspectRatio-b.jpeg)

#### Test \#2: Flat-plate wing: plunging

![alt text](./Figures/2-UVLM-FlatPlateWing-Plunging.jpeg)

#### Test \#3: Flat-plate wing: pitching

![alt text](./Figures/2-UVLM-FlatPlateWing-Pitching.jpeg)

[comment]: # (########################################################)



[comment]: # (########################################################)
## The dynamics of a flexible body
In progress...

### The fluid-structure coupling
In progress...

### Static aeroelasticity
In progress...

[comment]: # (########################################################)



[comment]: # (########################################################)
## References
1. Katz, J., & Plotkin, A. (2001). _Low-speed aerodynamics_ (Vol. 13). Cambridge University Press.

If you use data or code from pysco/Applications/Aeroelasticity, consider citing the paper below:

2. Gulizzi, V., & Benedetti, I. (2024). Computational aeroelastic analysis of wings based on the structural discontinuous Galerkin and aerodynamic vortex lattice methods. _Aerospace Science and Technology_, 144, 108808. [doi](https://doi.org/10.1016/j.ast.2023.108808)

[comment]: # (########################################################)