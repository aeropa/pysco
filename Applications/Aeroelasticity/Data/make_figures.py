# -*- coding: utf-8 -*-

# PYTHON IMPORTS ######################################################
import os
import sys
import numpy
import scipy
import scipy.interpolate
import matplotlib
import matplotlib.pyplot as plt
#######################################################################



# PYSCO IMPORTS #######################################################
this_file_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.abspath(os.path.join(this_file_path, "..", "..", "..", "src")))

from pysco.utilities.convert import pts_to_in
from pysco.utilities.io import InputReader
#######################################################################



# GENERAL DATA ########################################################
inputs = InputReader()

plot_options = {"fontsize": 10, "labelsize": 8, "markersize": 6}

# WHICH TEST
if (len(sys.argv) == 1):
    sys.argv.append("")

VLM_validation_1 = (sys.argv[1] == "VLM_validation_1")
VLM_validation_2 = (sys.argv[1] == "VLM_validation_2")
VLM_validation_3 = (sys.argv[1] == "VLM_validation_3")
VLM_validation_4 = (sys.argv[1] == "VLM_validation_4")
VLM_validation_5 = (sys.argv[1] == "VLM_validation_5")

UVLM_validation_1 = (sys.argv[1] == "UVLM_validation_1")
UVLM_validation_2 = (sys.argv[1] == "UVLM_validation_2")
UVLM_validation_3 = (sys.argv[1] == "UVLM_validation_3")
#######################################################################



# VALIDATION TEST #1.1: COMPARISON WITH LIFTING-LINE THEORY SOLUTION ##
if (VLM_validation_1):
    # DATA ============================================================
    filepath = os.path.join(".", "1-VLM-FlatPlateWing-EffectOfMesh.txt")
    inputs.load_file(filepath)

    b = inputs["Wing"]["Geometry"]["span"]
    # =================================================================


    # MAKE FIGURE: CONVERGENCE ========================================
    # OPEN FIGURE
    fig = plt.figure()
    ax = plt.gca()

    # PLOTS
    y = numpy.linspace(-0.5*b, 0.5*b, num = 1001)
    for n in (2, 4, 8, 32, 128):
        sol = inputs["Validation1"]["PresentSolution"]
        yn = numpy.array(sol['(1, %d)' % n]['y [m]'])
        gamma = numpy.array(sol['(1, %d)' % n]['Gamma [m/s^2]'])

        f = scipy.interpolate.interp1d(yn, gamma, kind = 'nearest', fill_value = 'extrapolate')
        ax.plot(2*y/b, f(y), label = "$\ell = b/%d$" % n)

    # AXES PROPERTIES
    ax.set_xlabel(r"$2y/b$", fontsize = plot_options["fontsize"])
    ax.set_ylabel(r"$\Gamma(y)$ [m$^2$/s]", fontsize = plot_options["fontsize"])

    ax.set_xlim([-1.0, 1.0])
    ax.set_ylim([0.0, 0.6])

    ax.set_xticks([-1.0, -0.5, 0.0, +0.5, 1.0])

    # LEGEND
    ax.legend(fontsize = plot_options["labelsize"], frameon = False, loc = 'lower center')

    # SAVE FIGURE
    for fmt in ["jpeg"]:
        fig.set_size_inches(pts_to_in(360), pts_to_in(180))
        plt.savefig(os.path.join("..", "Figures", "1-VLM-FlatPlateWing-EffectOfMesh-a."+fmt), format = fmt, bbox_inches = 'tight')
    # =================================================================


    # MAKE FIGURE: COMPARISON WITH LIFTING-LINE THEORY SOLUTION =======
    # OPEN FIGURE
    fig = plt.figure()
    ax = plt.gca()

    # PLOTS
    ref = inputs["Validation1"]["ReferenceSolution"]
    y, gamma = map(numpy.array, [ref["y [m]"], ref["Gamma [m/s^2]"]])
    ax.plot(2*y/b, gamma, ls = '-', lw = 2, color = 'k', label = "Lifting-line theory")
    ax.plot(-2*y/b, gamma, ls = '-', lw = 2, color = 'k')

    n = 128
    sol = inputs["Validation1"]["PresentSolution"]
    yn = numpy.array(sol['(1, %d)' % n]['y [m]'])
    gamma = numpy.array(sol['(1, %d)' % n]['Gamma [m/s^2]'])
    y = numpy.linspace(-0.5*b, 0.5*b, num = 1001)
    f = scipy.interpolate.interp1d(yn, gamma, kind = 'nearest', fill_value = 'extrapolate')
    ax.plot(2*y/b, f(y), label = "VLM, $\ell = b/%d$" % n)

    # AXES PROPERTIES
    ax.set_xlabel(r"$2y/b$", fontsize = plot_options["fontsize"])
    ax.set_ylabel(r"$\Gamma(y)$ [m$^2$/s]", fontsize = plot_options["fontsize"])

    ax.set_xlim([-1.0, 1.0])
    ax.set_ylim([0.0, 0.6])

    ax.set_xticks([-1.0, -0.5, 0.0, +0.5, 1.0])

    # LEGEND
    ax.legend(fontsize = plot_options["labelsize"], frameon = False, loc = 'lower center')

    # SAVE FIGURE
    for fmt in ["jpeg"]:
        fig.set_size_inches(pts_to_in(360), pts_to_in(180))
        plt.savefig(os.path.join("..", "Figures", "1-VLM-FlatPlateWing-EffectOfMesh-b."+fmt), format = fmt, bbox_inches = 'tight')
    # =================================================================
#######################################################################



# VALIDATION TEST #1.2: COMPARISON WITH 2D FLAT-PLATE AIRFOIL SOLUTION 
if (VLM_validation_2):
    # DATA ============================================================
    filepath = os.path.join(".", "1-VLM-FlatPlateWing-EffectOfMesh.txt")
    inputs.load_file(filepath)

    c = inputs["Wing"]["Geometry"]["chord_at_root"]
    # =================================================================


    # MAKE FIGURE: CONVERGENCE ========================================
    # OPEN FIGURE
    fig = plt.figure()
    ax = plt.gca()

    # PLOTS
    x = numpy.linspace(0.25, -0.75, num = 1001)
    for n in (1, 2, 4, 8, 16):
        sol = inputs["Validation2"]["PresentSolution"]
        delta = c/n
        xn = numpy.array(sol['(%d, %d)' % (n, 2*n*10)]['x [m]'])-0.25*delta
        dcp = numpy.array(sol['(%d, %d)' % (n, 2*n*10)]['dcp [-]'])

        if (len(xn) == 1):
            ax.plot(x, dcp[0]*numpy.ones_like(x), label = "$\delta = c$")
        else:
            f = scipy.interpolate.interp1d(xn, dcp, kind = 'nearest', fill_value = 'extrapolate')
            ax.plot(x, f(x), label = "$\delta = c/%d$" % n)
    
    # AXES PROPERTIES
    ax.set_xlabel(r"$x/c$", fontsize = plot_options["fontsize"])
    ax.set_ylabel(r"$-\Delta c_p(x,y = 0,z = 0)$", fontsize = plot_options["fontsize"])

    ax.set_xlim([+0.25, -0.75])
    ax.set_ylim([0.0, 5.0])

    ax.set_xticks([+0.25, 0.0, -0.25, -0.50, -0.75])

    # LEGEND
    ax.legend(fontsize = plot_options["labelsize"], frameon = False, loc = 'upper right')

    # SAVE FIGURE
    for fmt in ["jpeg"]:
        fig.set_size_inches(pts_to_in(360), pts_to_in(180))
        plt.savefig(os.path.join("..", "Figures", "1-VLM-FlatPlateWing-EffectOfMesh-c."+fmt), format = fmt, bbox_inches = 'tight')
    # =================================================================


    # MAKE FIGURE: COMPARISON WITH 2D FLAT-PLATE AIRFOIL SOLUTION =====
    # OPEN FIGURE
    fig = plt.figure()
    ax = plt.gca()

    # PLOTS
    ref = inputs["Validation2"]["ReferenceSolution"]
    x, dcp = map(numpy.array, [ref["x [m]"], ref["dcp [-]"]])
    ax.plot(-(x-0.25), dcp, ls = '-', lw = 2, color = 'k', label = "Flat-plate solution")

    n = 16
    sol = inputs["Validation2"]["PresentSolution"]
    xn = numpy.array(sol['(%d, %d)' % (n, 2*n*10)]['x [m]'])-0.25*delta
    dcp = numpy.array(sol['(%d, %d)' % (n, 2*n*10)]['dcp [-]'])
    x = numpy.linspace(0.25, -0.75, num = 1001)
    f = scipy.interpolate.interp1d(xn, dcp, kind = 'nearest', fill_value = 'extrapolate')
    ax.plot(x, f(x), label = "VLM, $\delta = c/%d$" % n)
    
    # AXES PROPERTIES
    ax.set_xlabel(r"$x/c$", fontsize = plot_options["fontsize"])
    ax.set_ylabel(r"$-\Delta c_p(x,y = 0,z = 0)$", fontsize = plot_options["fontsize"])

    ax.set_xlim([+0.25, -0.75])
    ax.set_ylim([0.0, 5.0])

    ax.set_xticks([+0.25, 0.0, -0.25, -0.50, -0.75])

    # LEGEND
    ax.legend(fontsize = plot_options["labelsize"], frameon = False, loc = 'upper right')

    # SAVE FIGURE
    for fmt in ["jpeg"]:
        fig.set_size_inches(pts_to_in(360), pts_to_in(180))
        plt.savefig(os.path.join("..", "Figures", "1-VLM-FlatPlateWing-EffectOfMesh-d."+fmt), format = fmt, bbox_inches = 'tight')
    # =================================================================
#######################################################################



# VALIDATION TEST #1.3: EFFECT OF SWEEP ANGLE ON CL DISTRIBUTION ######
if (VLM_validation_3):
    # DATA ============================================================
    filepath = os.path.join(".", "1-VLM-FlatPlateWing-EffectOfGeometry.txt")
    inputs.load_file(filepath)

    b = inputs["Validation3"]["Wing"]["Geometry"]["span"]
    # =================================================================


    # MAKE FIGURE: EFFECT OF SWEEP ANGLE ON CL DISTRIBUTION ===========
    # OPEN FIGURE
    fig = plt.figure()
    ax = plt.gca()

    # PLOTS
    for lam in ("0", "45", "-45"):
        sol = inputs["Validation3"]["ReferenceSolution"][lam]
        y = numpy.array(sol['2y/b [%]'])
        cl_cL = numpy.array(sol['cl/cL [-]'])

        if (lam == '0'):
            #ax.plot(y/100.0, cl_cL, label = "$\Lambda = %s^\circ$ (Ref.[1])" % lam)
            ax.plot(y/100.0, cl_cL, color = 'k', label = "Ref.[1]")
        else:
            ax.plot(y/100.0, cl_cL, color = 'k')
    
    for lam in ("0", "45", "-45"):
        sol = inputs["Validation3"]["PresentSolution"][lam]
        y = numpy.array(sol['y [m]'])
        cl_cL = numpy.array(sol['cl/cL [-]'])

        ax.plot(2*y/b, cl_cL, marker = '.', label = "$\Lambda = %s^\circ$" % lam)
    
    # AXES PROPERTIES
    ax.set_xlabel(r"$2y/b$", fontsize = plot_options["fontsize"])
    ax.set_ylabel(r"$c_l/c_L$", fontsize = plot_options["fontsize"])

    ax.set_xlim([0.0, 1.0])
    ax.set_ylim([0.0, 1.5])

    ax.set_xticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])

    # LEGEND
    ax.legend(fontsize = plot_options["labelsize"], frameon = False, loc = 'lower left')

    # SAVE FIGURE
    for fmt in ["jpeg"]:
        fig.set_size_inches(pts_to_in(360), pts_to_in(180))
        plt.savefig(os.path.join("..", "Figures", "1-VLM-FlatPlateWing-EffectOfSweepAngle-a."+fmt), format = fmt, bbox_inches = 'tight')
    # =================================================================
#######################################################################



# VALIDATION TEST #1.4: EFFECT OF ASPECT RATIO AND SWEEP ANGLE ON CLa #
if (VLM_validation_4):
    # DATA ============================================================
    filepath = os.path.join(".", "1-VLM-FlatPlateWing-EffectOfGeometry.txt")
    inputs.load_file(filepath)
    # =================================================================


    # MAKE FIGURE: EFFECT OF ASPECT RATIO AND SWEEP ANGLE ON CLa ======
    # OPEN FIGURE
    fig = plt.figure()
    ax = plt.gca()

    # PLOTS
    AR = numpy.array([0.0, 2.0])
    c_L_alpha = 0.5*numpy.pi*AR
    ax.plot(AR, c_L_alpha, color = 'k', ls = '--', label = '$\pi b^2/(2S)$')

    for lam in ("0", "30", "45", "60"):
        sol = inputs["Validation4"]["ReferenceSolution"][lam]
        AR = numpy.array(sol['b^2/S [-]'])
        c_L_alpha = numpy.array(sol['c_L_alpha [-]'])

        if (lam == '0'):
            ax.plot(AR, c_L_alpha, color = 'k', label = "Ref.[1]")
        else:
            ax.plot(AR, c_L_alpha, color = 'k')

    for lam in ("0", "30", "45", "60"):
        sol = inputs["Validation4"]["PresentSolution"][lam]
        AR = numpy.array(sol['b^2/S [-]'])
        c_L_alpha = numpy.array(sol['c_L_alpha [-]'])

        ax.plot(AR, c_L_alpha, marker = '.', label = "$\Lambda = %s^\circ$" % lam)
    
    # AXES PROPERTIES
    ax.set_xlabel(r"$b^2/S$", fontsize = plot_options["fontsize"])
    ax.set_ylabel(r"$c_{L_\alpha}$", fontsize = plot_options["fontsize"])

    ax.set_xlim([0.0, 9.0])
    ax.set_ylim([0.0, 6.0])

    # LEGEND
    ax.legend(fontsize = plot_options["labelsize"], frameon = False, loc = 'upper left', ncol = 3)

    # SAVE FIGURE
    for fmt in ["jpeg"]:
        fig.set_size_inches(pts_to_in(360), pts_to_in(180))
        plt.savefig(os.path.join("..", "Figures", "1-VLM-FlatPlateWing-EffectOfAspectRatioAndSweepAngle-a."+fmt), format = fmt, bbox_inches = 'tight')
    # =================================================================
#######################################################################



# VALIDATION TEST #1.5: EFFECT OF TAPER RATIO #########################
if (VLM_validation_5):
    # DATA ============================================================
    filepath = os.path.join(".", "1-VLM-FlatPlateWing-EffectOfGeometry.txt")
    inputs.load_file(filepath)
    # =================================================================


    # MAKE FIGURE: EFFECT OF TAPER RATIO ON CL DISTRIBUTION ===========
    # OPEN FIGURE
    fig = plt.figure()
    ax = plt.gca()

    # PLOTS
    for TR in ("1.0", "0.6", "0.4"):
        sol = inputs["Validation5"]["ReferenceSolution"][TR]
        y = numpy.array(sol['2y/b [-]'])
        cl_cL = numpy.array(sol['cl/cL [-]'])

        if (TR == '1.0'):
            ax.plot(y, cl_cL, color = 'k', label = "Ref.[1]")
        else:
            ax.plot(y, cl_cL, color = 'k')
    
    for TR in ("1.0", "0.6", "0.4"):
        sol = inputs["Validation5"]["PresentSolution"][TR]
        y = numpy.array(sol['2y/b [-]'])
        cl_cL = numpy.array(sol['cl/cL [-]'])

        ax.plot(y, cl_cL, marker = '.', markevery = 4, label = "$c_T/c_R = %s$" % TR)
    
    # AXES PROPERTIES
    ax.set_xlabel(r"$2y/b$", fontsize = plot_options["fontsize"])
    ax.set_ylabel(r"$c_l/c_L$", fontsize = plot_options["fontsize"])

    ax.set_xlim([0.0, 1.0])
    ax.set_ylim([0.0, 1.5])

    ax.set_xticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])

    # LEGEND
    ax.legend(fontsize = plot_options["labelsize"], frameon = False, loc = 'lower left')

    # SAVE FIGURE
    for fmt in ["jpeg"]:
        fig.set_size_inches(pts_to_in(360), pts_to_in(180))
        plt.savefig(os.path.join("..", "Figures", "1-VLM-FlatPlateWing-EffectOfTaperRatio-a."+fmt), format = fmt, bbox_inches = 'tight')
    # =================================================================
#######################################################################



# VALIDATION TEST #2.1: EFFECT OF ASPECT RATIO ########################
if (UVLM_validation_1):
    # DATA ============================================================
    filepath = os.path.join(".", "2-UVLM-FlatPlateWing-EffectOfGeometry.txt")
    inputs.load_file(filepath)
    # =================================================================


    # MAKE FIGURE: EFFECT OF ASPECT RATIO ON CL =======================
    # OPEN FIGURE
    fig = plt.figure()
    ax = plt.gca()

    # PLOTS
    for AR in ("4", "8", "12"):
        sol = inputs["Validation1"]["ReferenceSolution"][AR]
        tau = numpy.array(sol['V_inf*t/c (c_L) [-]'])
        cL = numpy.array(sol['c_L [-]'])

        if (AR == '4'):
            ax.plot(tau, cL, color = 'k', label = "Ref.[1]")
        else:
            ax.plot(tau, cL, color = 'k')

    for AR in ("4", "8", "12"):
        sol = inputs["Validation1"]["PresentSolution"][AR]
        tau = numpy.array(sol['V_inf*t/c [-]'])
        cL = numpy.array(sol['c_L [-]'])

        ax.plot(tau, cL, marker = '.', markevery = 4, label = "$b^2/S = %s$" % AR)
    
    # AXES PROPERTIES
    ax.set_xlabel(r"$V_\infty t/c$", fontsize = plot_options["fontsize"])
    ax.set_ylabel(r"$c_L$", fontsize = plot_options["fontsize"])

    ax.set_xlim([0.0, 10.0])
    ax.set_ylim([0.2, 0.5])

    ax.set_xticks([0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])

    # LEGEND
    ax.legend(fontsize = plot_options["labelsize"], frameon = False, loc = 'lower right', ncol = 2)

    # SAVE FIGURE
    for fmt in ["jpeg"]:
        fig.set_size_inches(pts_to_in(360), pts_to_in(180))
        plt.savefig(os.path.join("..", "Figures", "2-UVLM-FlatPlateWing-EffectOfAspectRatio-a."+fmt), format = fmt, bbox_inches = 'tight')
    # =================================================================

    # MAKE FIGURE: EFFECT OF ASPECT RATIO ON CL =======================
    # OPEN FIGURE
    fig = plt.figure()
    ax = plt.gca()

    # PLOTS
    for AR in ("4", "8", "12"):
        sol = inputs["Validation1"]["ReferenceSolution"][AR]
        tau = numpy.array(sol['V_inf*t/c (c_D) [-]'])
        cD = numpy.array(sol['c_D [-]'])

        if (AR == '4'):
            ax.plot(tau, cD, color = 'k', label = "Ref.[1]")
        else:
            ax.plot(tau, cD, color = 'k')

    for AR in ("4", "8", "12"):
        sol = inputs["Validation1"]["PresentSolution"][AR]
        tau = numpy.array(sol['V_inf*t/c [-]'])
        cD = numpy.array(sol['c_D [-]'])

        ax.plot(tau, cD, marker = '.', markevery = 4, label = "$b^2/S = %s$" % AR)
    
    # AXES PROPERTIES
    ax.set_xlabel(r"$V_\infty t/c$", fontsize = plot_options["fontsize"])
    ax.set_ylabel(r"$c_D$", fontsize = plot_options["fontsize"])

    ax.set_xlim([0.0, 10.0])
    ax.set_ylim([0.0, 0.02])

    ax.set_xticks([0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0])

    # LEGEND
    ax.legend(fontsize = plot_options["labelsize"], frameon = False, loc = 'upper right', ncol = 2)

    # SAVE FIGURE
    for fmt in ["jpeg"]:
        fig.set_size_inches(pts_to_in(360), pts_to_in(180))
        plt.savefig(os.path.join("..", "Figures", "2-UVLM-FlatPlateWing-EffectOfAspectRatio-b."+fmt), format = fmt, bbox_inches = 'tight')
    # =================================================================
#######################################################################



# AUXILIARY ROUTINES ##################################################
def eval_cl_cm_Theodorsen(t, rho, V, c, a, h0, alpha0, k):
    omega = 2.0*k*V/c
    h = h0*numpy.sin(omega*t)
    dh = h0*omega*numpy.cos(omega*t)
    ddh = -h0*omega*omega*numpy.sin(omega*t)
    alpha = alpha0*numpy.sin(omega*t)
    dalpha = alpha0*omega*numpy.cos(omega*t)
    ddalpha = -alpha0*omega*omega*numpy.sin(omega*t)

    Kn = scipy.special.kn
    C = Kn(1,k)/(Kn(0,k)+Kn(1,k))
    
    l = V*C*(V*alpha-dh+(0.75-a/c)*c*dalpha)+0.25*c*(V*dalpha-ddh+(0.5-a/c)*c*ddalpha)
    l *= rho*numpy.pi*c

    cl = l/(0.5*rho*V*V*c)
    cm = numpy.zeros_like(cl)
    
    return cl, cm
#######################################################################



# VALIDATION TEST #2.2: WING PLUNGING #################################
if (UVLM_validation_2):
    # DATA ============================================================
    filepath = os.path.join(".", "2-UVLM-FlatPlateWing-Motion.txt")
    inputs.load_file(filepath)

    rho = inputs["ProblemSettings"]["Free-stream conditions"]["density"]
    V = inputs["ProblemSettings"]["Free-stream conditions"]["velocity"]
    V = numpy.linalg.norm(V)
    c = inputs["Validation2"]["Wing"]["Geometry"]["chord_at_root"]

    h0 = 0.1
    alpha0 = 0.0
    # =================================================================


    # MAKE FIGURE: CL =================================================
    # OPEN FIGURE
    fig = plt.figure()
    ax = plt.gca()

    # PLOTS
    for k in ("0.2", "0.6"):
        t = numpy.array(inputs["Validation2"]["PresentSolution"][k]["t [s]"])
        tau = V*t/c
        cl, cm = eval_cl_cm_Theodorsen(t, rho, V, c, 0.0, h0, alpha0, float(k))
        
        if (k == '0.2'):
            ax.plot(tau, cl, color = 'k', label = "Theodorsen")
        else:
            ax.plot(tau, cl, color = 'k')

    for k in ("0.2", "0.6"):
        sol = inputs["Validation2"]["PresentSolution"][k]
        t = numpy.array(sol['t [s]'])
        tau = V*t/c
        cl = numpy.array(sol['c_l [-]'])

        if (k == '0.2'):
            ax.plot(tau, cl, marker = '.', markevery = 4, label = "UVLM ($k = %s$)" % k)
        elif (k == '0.6'):
            ax.plot(tau, cl, marker = '.', markevery = 2, label = "UVLM ($k = %s$)" % k)
    
    # AXES PROPERTIES
    ax.set_xlabel(r"$V_\infty t/c$", fontsize = plot_options["fontsize"])
    ax.set_ylabel(r"$c_l$", fontsize = plot_options["fontsize"])

    ax.set_xlim([0.0, 30.0])
    ax.set_ylim([-0.8, 0.8])

    ax.set_yticks([-0.8, -0.4, 0.0, 0.4, 0.8])

    # LEGEND
    ax.legend(fontsize = plot_options["labelsize"], frameon = False, loc = 'upper right', ncol = 3)

    # SAVE FIGURE
    for fmt in ["jpeg"]:
        fig.set_size_inches(pts_to_in(360), pts_to_in(180))
        plt.savefig(os.path.join("..", "Figures", "2-UVLM-FlatPlateWing-Plunging."+fmt), format = fmt, bbox_inches = 'tight')
    # =================================================================
#######################################################################



# VALIDATION TEST #2.3: WING PITCHING #################################
if (UVLM_validation_3):
    # DATA ============================================================
    filepath = os.path.join(".", "2-UVLM-FlatPlateWing-Motion.txt")
    inputs.load_file(filepath)

    rho = inputs["ProblemSettings"]["Free-stream conditions"]["density"]
    V = inputs["ProblemSettings"]["Free-stream conditions"]["velocity"]
    V = numpy.linalg.norm(V)
    c = inputs["Validation2"]["Wing"]["Geometry"]["chord_at_root"]

    h0 = 0.0
    alpha0 = 2.0*numpy.pi/180.0
    # =================================================================


    # MAKE FIGURE: CL =================================================
    # OPEN FIGURE
    fig = plt.figure()
    ax = plt.gca()

    # PLOTS
    for k in ("0.2", "0.6"):
        t = numpy.array(inputs["Validation3"]["PresentSolution"][k]["t [s]"])
        tau = V*t/c
        cl, cm = eval_cl_cm_Theodorsen(t, rho, V, c, 0.25*c, h0, alpha0, float(k))
        
        if (k == '0.2'):
            ax.plot(tau, cl, color = 'k', label = "Theodorsen")
        else:
            ax.plot(tau, cl, color = 'k')

    for k in ("0.2", "0.6"):
        sol = inputs["Validation3"]["PresentSolution"][k]
        t = numpy.array(sol['t [s]'])
        tau = V*t/c
        cl = numpy.array(sol['c_l [-]'])

        if (k == '0.2'):
            ax.plot(tau, cl, marker = '.', markevery = 4, label = "UVLM ($k = %s$)" % k)
        elif (k == '0.6'):
            ax.plot(tau, cl, marker = '.', markevery = 2, label = "UVLM ($k = %s$)" % k)
    
    # AXES PROPERTIES
    ax.set_xlabel(r"$V_\infty t/c$", fontsize = plot_options["fontsize"])
    ax.set_ylabel(r"$c_l$", fontsize = plot_options["fontsize"])

    ax.set_xlim([0.0, 30.0])
    ax.set_ylim([-0.4, 0.4])

    ax.set_yticks([-0.4, -0.2, 0.0, 0.2, 0.4])

    # LEGEND
    ax.legend(fontsize = plot_options["labelsize"], frameon = False, loc = 'upper right', ncol = 3)

    # SAVE FIGURE
    for fmt in ["jpeg"]:
        fig.set_size_inches(pts_to_in(360), pts_to_in(180))
        plt.savefig(os.path.join("..", "Figures", "2-UVLM-FlatPlateWing-Pitching."+fmt), format = fmt, bbox_inches = 'tight')
    # =================================================================
#######################################################################