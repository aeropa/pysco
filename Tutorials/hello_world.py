# -*- coding: utf-8 -*-

# PYTHON IMPORTS ######################################################
import os
import sys
#######################################################################



# PYSCO IMPORTS #######################################################
this_file_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.abspath(os.path.join(this_file_path, "..", "src")))

import pysco.utilities.io
#######################################################################



# SAY HELLO ###########################################################
pysco.utilities.io.print_to_screen("Hello, world!")
#######################################################################