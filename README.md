# PySCo

## Description
**PySCo** is a collection of **Py**thon routines for **S**cientific **Co**mputing.

## Graphical Abstract
![alt text](./Ad/GraphicalAbstract.jpeg)

## Installation
Routines may be provided upon request contacting [Vincenzo Gulizzi](mailto:vincenzo.gulizzi@unipa.it).

## Authors and acknowledgment
Main developers: [Vincenzo Gulizzi](mailto:vincenzo.gulizzi@unipa.it)

We acknowledge Chris Rycroft, Romain Quey, and Robert Saye for their support in the use of [VORO++](https://math.lbl.gov/voro++/), [Neper](https://neper.info), and [Algoim](https://algoim.github.io), respectively.

## License
Copyright (c) 2018 The Python Packaging Authority

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Project status
In progress.

## Applications
Below the list of Journal and Conference papers that have used PySCo.
### Journal papers
* Gulizzi, V., Benedetti, I., & Milazzo, A. (2024). High-order accurate transient and free-vibration analysis of plates and shells. _Journal of Sound and Vibration_, 587, 118479. [doi](https://doi.org/10.1016/j.jsv.2024.118479)
* Gulizzi, V., & Benedetti, I. (2024). Computational aeroelastic analysis of wings based on the structural discontinuous Galerkin and aerodynamic vortex lattice methods. _Aerospace Science and Technology_, 144, 108808. [doi](https://doi.org/10.1016/j.ast.2023.108808)
* Gulizzi, V., Benedetti, I., & Milazzo, A. (2023). High-order Accurate Beam Models Based on Discontinuous Galerkin Methods. _Aerotecnica Missili & Spazio_, 1-16. [doi](https://doi.org/10.1007/s42496-023-00168-3)
* Guarino, G., Gulizzi, V., & Milazzo, A. (2022). Accurate multilayered shell buckling analysis via the implicit-mesh discontinuous Galerkin method. _AIAA Journal_, 60(12), 6854-6868. [doi](https://doi.org/10.2514/1.J061933)
* Guarino, G., Gulizzi, V., & Milazzo, A. (2021). High-fidelity analysis of multilayered shells with cut-outs via the discontinuous Galerkin method. _Composite Structures_, 276, 114499. [doi](https://doi.org/10.1016/j.compstruct.2021.114499)
* Guarino, G., Milazzo, A., & Gulizzi, V. (2021). Equivalent-Single-Layer discontinuous Galerkin methods for static analysis of multilayered shells. _Applied Mathematical Modelling_, 98, 701-721. [doi](https://doi.org/10.1016/j.apm.2021.05.024)
* Benedetti, I., Gulizzi, V., & Milazzo, A. (2020). Layer-wise discontinuous Galerkin methods for piezoelectric laminates. _Modelling_, 1(2), 198-214. [doi](https://doi.org/10.3390/modelling1020012)
* Gulizzi, V., Benedetti, I., & Milazzo, A. (2020). An implicit mesh discontinuous Galerkin formulation for higher-order plate theories. _Mechanics of Advanced Materials and Structures_, 27(17), 1494-1508. [doi](https://doi.org/10.1080/15376494.2018.1516258)
* Gulizzi, V., Benedetti, I., & Milazzo, A. (2020). A high-resolution layer-wise discontinuous Galerkin formulation for multilayered composite plates. _Composite Structures_, 242, 112137. [doi](https://doi.org/10.1016/j.compstruct.2020.112137)
### Conference papers
* Gulizzi, V. (2024). Elasto-acoustic Floquet-Bloch problems solved via discontinuous Galerkin methods. In _30th AIAA/CEAS Aeroacoustics Conference_, Rome, Italy. [doi](https://doi.org/10.2514/6.2024-3401)
* Montano, F., Benedetti, I., & Gulizzi, V. (2023). Stability and flying qualities analysis of flexible aircraft by coupling the Vortex Lattice method and the discontinuous Galerkin method. In _Aerospace Europe Conference 2023 – 10TH EUCASS – 9TH CEAS_, Lausanne, Switzerland. [link](https://www.eucass.eu/component/docindexer/?task=download&id=6724)
* Guarino, G., Gulizzi, V., & Milazzo, A. (2022). TRANSIENT AND FREE-VIBRATION ANALYSIS OF LAMINATED SHELLS THROUGH THE DISCONTINUOUS GALERKIN METHOD. In _ICAS PROCEEDINGS 33th Congress of the International Council of the Aeronautical Sciences_ Stockholm, Sweden. [link](https://www.icas.org/ICAS_ARCHIVE/ICAS2022/data/papers/ICAS2022_0427_paper.pdf)
* Guarino, G., Milazzo, A., & Gulizzi, V. (2022). Buckling analysis of multilayered structures using high-order theories and the implicit-mesh discontinuous Galerkin method. In _AIAA SCITECH 2022 Forum_ (p. 1490). [doi](https://doi.org/10.2514/6.2022-1490)
* Gulizzi, V., Benedetti, I., & Milazzo, A. (2020). A discontinuous Galerkin formulation for variable angle tow composite plates higher-order theories. In _Proceedings of the 6th. European Conference on Computational Mechanics (Solids, Structures and Coupled Problems) ECCM 6 7th. European Conference on Computational Fluid Dynamics ECFD 7_ (pp. 243-254). [link](https://congress.cimne.com/eccm_ecfd2018/admin/files/filePaper/p1167.pdf).