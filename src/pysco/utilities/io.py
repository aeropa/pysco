# -*- coding: utf-8 -*-

# PYTHON IMPORTS ######################################################
import ast
#######################################################################



# PYSCO IMPORTS #######################################################
from .mpi import ParallelWorld
#######################################################################



# GLOBAL PARAMETERS / VARIABLES / DATA STRUCTURES #####################
parallel_world = ParallelWorld()
#######################################################################



# PRINT TO SCREEN #####################################################
def print_to_screen(*args, **kwargs):
    if ("rank" in kwargs):
        if (parallel_world.rank == kwargs["rank"]):
            kwargs.pop("rank")
            print(*args, **kwargs)
    else:
        if (parallel_world.rank_handles_io()):
            print(*args, **kwargs)
#######################################################################



# INPUT READER ########################################################
class InputReader(dict):
    # CONSTRUCTOR =====================================================
    def __init__(self, **params):
        self.content = ""
        self.hierarchy = []
    # =================================================================


    # AUXILIARY METHODS ===============================================
    def line_is_empty(self, line):
        if (line == ''):
            return True
        for c in line:
            if (c != ' '):
                return False
        return True

    def find_first_not_of(self, line, c):
        line_len = len(line)
        pos = 0
        while ((pos < line_len) and (line[pos] == c)):
            pos += 1
        if (pos == line_len): pos = -1
        return pos
    
    def remove_comments(self, string):
        # REMOVE LINES STARTING BY '#' --------------------------------
        pos = 0
        posA = string.find('#', pos)
        while (posA != -1):
            posB = string.find('\n', (posA+1))

            if (posB == -1):
                msg  = "ERROR: InputReader.remove_comments\n"
                msg += "| Could not find the new line character paired with '#' in the string.\n"
                raise Exception(msg)

            string = string[:posA]+string[posB:]

            pos = (posA+1)

            posA = string.find('#', pos)
        # -------------------------------------------------------------

        return string
    
    def remove_n(self, string, cA, cB):
        '''
        Remove new line characters between two characters in a string.
        '''
        
        pos = 0
        posA = string.find(cA, pos)
        while (posA != -1):
            posB = string.find(cB, (posA+1))

            if (posB == -1):
                msg  = "ERROR: InputReader.remove_n\n"
                msg += "| Could not find the character "+str(cB)+" paired with "+str(cA)+" in the string.\n"
                raise Exception(msg)

            string = string[:posA]+string[posA:posB].replace('\n', '')+string[posB:]

            pos = (posA+1)
            
            posA = string.find(cA, pos)

            if ((posA != -1) and (posA < posB)):
                msg  = "ERROR: InputReader.remove_n\n"
                msg += "| Could not find the character "+str(cB)+" paired with "+str(cA)+" in the string.\n"
                raise Exception(msg)

        return string

    def build_hierarchy(self, string):
        '''
        Build a hierarchy of strings from the indentation in a string.
        '''

        all_lines = string.split('\n')

        # REMOVE EMPTY LINES ------------------------------------------
        lines = []
        for k, line in enumerate(all_lines):
            if (not(self.line_is_empty(line))):
                lines.append(line)
        # -------------------------------------------------------------

        # BUILD THE HIERARCHY -----------------------------------------
        self.hierarchy = [[]]
        
        indentation = [0]
        depth = 0
        for line in lines:
            content = line.strip()
            indent_len = len(line)-len(content)
            
            if (indent_len > indentation[-1]):
                depth += 1
                indentation.append(indent_len)
                self.hierarchy.append([])
            
            elif (indent_len < indentation[-1]):
                while (indent_len < indentation[-1]):
                    depth -= 1
                    indentation.pop()
                    top = self.hierarchy.pop()
                    self.hierarchy[-1].append(top)

                if (indent_len != indentation[-1]):
                    msg  = "ERROR: InputReader.build_hierarchy\n"
                    msg += "| Unexpected indentation.\n"
                    raise Exception(msg)

            self.hierarchy[-1].append(content)
        
        while (len(self.hierarchy) > 1):
            top = self.hierarchy.pop()
            self.hierarchy[-1].append(top)
        
        self.hierarchy = self.hierarchy[-1]
        # -------------------------------------------------------------

        return self.hierarchy

    def set_key_value(self, d, h):
        n_entries = len(h)
        n = 0
        while (n < n_entries):
            entry = h[n]
            if (n == (n_entries-1)):
                key, value = entry.split(':')
                key, value = key.strip(), value.strip()
                d[key] = ast.literal_eval(value)
            else:
                next_entry = h[n+1]
                
                if (isinstance(entry, str) and isinstance(next_entry, list)):
                    key = entry.replace(':', '')
                    d[key] = {}
                    self.set_key_value(d[key], next_entry)
                    n += 1
                elif (isinstance(entry, str) and isinstance(next_entry, str)):
                    key, value = entry.split(':')
                    key, value = key.strip(), value.strip()
                    d[key] = ast.literal_eval(value)

            n += 1
    # =================================================================


    # GENERAL METHODS FOR LOADING FILES ===============================
    def load_file(self, filepath):
        fp = open(filepath, mode = 'r')
        whole_content = fp.read()
        whole_content += "\n"
        fp.close()

        # REMOVE COMMENTS ---------------------------------------------
        whole_content = self.remove_comments(whole_content)
        # -------------------------------------------------------------

        # REPLACE THE TAB CHARACTERS WITH FOUR SPACES -----------------
        whole_content.replace('\t', '    ')
        # -------------------------------------------------------------

        # HANDLE PARENTHESIS ------------------------------------------
        whole_content = self.remove_n(whole_content, '(', ')')
        whole_content = self.remove_n(whole_content, '[', ']')
        whole_content = self.remove_n(whole_content, '{', '}')
        # -------------------------------------------------------------

        # BUILD THE HIERARCHICAL CONTENT ------------------------------
        self.hierarchy = self.build_hierarchy(whole_content)
        # -------------------------------------------------------------

        # BUILD THE DICTIONARY ASSOCIATED WITH THE HIERARCHY ----------
        self.set_key_value(self, self.hierarchy)
        # -------------------------------------------------------------
    # =================================================================
#######################################################################