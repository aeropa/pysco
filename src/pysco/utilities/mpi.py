# -*- coding: utf-8 -*-

# PYTHON IMPORTS ######################################################
import numpy

import ctypes

try:
    from mpi4py import MPI
    from mpi4py.util import dtlib

    if MPI._sizeof(MPI.Comm) == ctypes.sizeof(ctypes.c_int):
        _MPI_Comm_type = ctypes.c_int
    else:
        _MPI_Comm_type = ctypes.c_void_p
    
    using_mpi = True

except ImportError:
    MPI = None
    _MPI_Comm_type = ctypes.c_void_p

    using_mpi = False
#######################################################################



# PYSCO IMPORTS #######################################################
#######################################################################



# BASE ################################################################
class ParallelWorld:
    # CLASS-WIDE CONSTANTS ============================================
    rank_io = 0
    # =================================================================


    # CONSTRUCTOR =====================================================
    def __init__(self, **params):
        if (using_mpi):
            self.comm = MPI.COMM_WORLD
            self.n_ranks = self.comm.Get_size()
            self.rank = self.comm.Get_rank()
        else:
            self.comm = None
            self.n_ranks = 1
            self.rank = 0
    # =================================================================


    # UTILITIES =======================================================
    def rank_handles_io(self):
        return (self.rank == self.rank_io) if (using_mpi) else True
    
    def barrier(self):
        if (using_mpi):
            self.comm.Barrier()
    # =================================================================


    # COLLECTIVE CALLS ================================================
    def broadcast(self, data, root):
        if (using_mpi):
            self.comm.Bcast(data, root = root)

    def all_gather(self, data):
        if (using_mpi):
            data_is_scalar = isinstance(data, (int, float, complex))
            data = numpy.array(data)
            mpi_data_type = dtlib.from_numpy_dtype(data.dtype)
            m = numpy.zeros((self.n_ranks, data.size), dtype = data.dtype)
            self.comm.Allgather([data, mpi_data_type], [m, mpi_data_type])
            if (data_is_scalar):
                return [mr[0] for mr in m]
            else:
                return [mr.reshape(data.shape) for mr in m]
        else:
            return [data]

    def all_reduce_max(self, data):
        if (using_mpi):
            data = numpy.array(data)
            mpi_data_type = dtlib.from_numpy_dtype(data.dtype)
            res = numpy.zeros_like(data)
            self.comm.Allreduce(data, res, op = MPI.MAX)
            return res
        else:
            return data

    def all_reduce_min(self, data):
        if (using_mpi):
            data = numpy.array(data)
            mpi_data_type = dtlib.from_numpy_dtype(data.dtype)
            res = numpy.zeros_like(data)
            self.comm.Allreduce(data, res, op = MPI.MIN)
            return res
        else:
            return data

    def all_reduce_sum(self, data):
        if (using_mpi):
            data = numpy.array(data)
            mpi_data_type = dtlib.from_numpy_dtype(data.dtype)
            res = numpy.zeros_like(data)
            self.comm.Allreduce(data, res, op = MPI.SUM)
            return res
        else:
            return data
    # =================================================================


    # ONE-SIDED COMMUNICATIONS ========================================
    def allocate_window(self, data):
        if (using_mpi):
            itemsize = dtlib.from_numpy_dtype(data.dtype).Get_size()
            window_size = data.size*itemsize
            window = MPI.Win.Allocate(size = window_size, disp_unit = itemsize, comm = self.comm)
            return window
        else:
            return None
    # =================================================================
#######################################################################