# PySCo: Solvers Module

**PySCo** has been designed for solving Partial Differential Equations (PDEs) using Discontinuous Galerkin (DG) methods.

A few examples of **PySCo** PDE solvers capabilities are given below.