# PySCo: Geometry Generation Module

**PySCo** allows users to generate geometries via explicit parametrizations and/or implicit descriptions (e.g. via level set functions).

A few examples of **PySCo** geometry generation capabilities are given below.

## Example 1: A rectangle
![alt text](./1-Geometry/1-1-Geometry.jpeg)

Consider a rectangular domain of length $L$ and height $H$ as sketched in the figure[^1] above.
A generic point $\boldsymbol{x} = (x, y)$ in the rectangle may be simply represented as
$$
\boldsymbol{x} = u\boldsymbol{e}_1+v\boldsymbol{e}_2,
$$
where $(u,v) \in [0,L]\times[0,H]$.

The code below shows how to introduce such a geometry in **PySCo**:
```python
# IMPORTS -------------------------------------------------------------
import pysco.geometry.base
# ---------------------------------------------------------------------

# GEOMETRY ------------------------------------------------------------
geo = pysco.geometry.base.Geometry(dim = 2)

L = 1.0
H = 0.5

geo.add_parametric_surface(ID = "RectangleGeo", expression = ("u", "v"), domain = ([0.0, L], [0.0, H]))
# ---------------------------------------------------------------------
```

## Example 2: A thick cylinder
![alt text](./1-Geometry/1-2-Geometry.jpeg)

A three-dimensional geometry can also be represented in paremetric form.
For example, consider a cylinder of length $L$, radius $R$ and thickness $t$, as shown above.
Its parametric representation may be given as follow
$$
\boldsymbol{x} = u\boldsymbol{e}_1+v\cos(w)\boldsymbol{e}_2+v\sin(w)\boldsymbol{e}_3,
$$
where $(u,v,w) \in [0,L]\times[R-t/2, R+t/2]\times[0, 2\pi]$.

In **PySCo**, similar to the preceding example, one uses the following code:
```python
# IMPORTS -------------------------------------------------------------
import numpy
import pysco.geometry.base
# ---------------------------------------------------------------------

# GEOMETRY ------------------------------------------------------------
pi = numpy.pi

geo = pysco.geometry.base.Geometry(dim = 3)

L = 1.0
R = 0.5
t = 0.02

geo.add_parametric_volume(ID = "CylinderGeo", expression = ("u", "v*cos(w)", "v*sin(w)"), domain = ([0.0, L], [R-0.5*t, R+0.5*t], [0.0, 2.0*pi]))
# ---------------------------------------------------------------------
```

## Example 3: Two equivalent squares with a circular hole
![alt text](./1-Geometry/1-3-Geometries.jpeg)

As a third example, we show how **PySCo** can work with both explicit and implicit representation of the same geometry.
Figure (a) above represents a square of size $L$ with a central circular hole of radius $R$.
Such a geometry, denoted by $\mathscr{D}$, may be represented in **PySCo** either explicitly using a NURBS-based parametrization or implicitly via a level set function.

### NURBS-based parametrization
The explicit parametrization of the geometry shown in figure (a) is given using a Non-uniform rational B-spline, or NURBS, by expressing a generic point $\boldsymbol{x}\in\mathscr{D}$ as follows
$$
\boldsymbol{x} = \sum_{i}\sum_{j}R^{(i,j)}(u,v)\boldsymbol{x}^{(i,j)},
$$
where $(u,v) \in [0,1]\times[0,1]$, $R^{(i,j)}(u,v)$ is the $(i,j)$-th non-uniform rational basis function, and $\boldsymbol{x}^{(i,j)}$ is the $(i,j)$-th control point.
The location of the control points is reported in figure (b).
The interested readers are referred to The NURBS books[^2] by Piegl and Tiller for further details about NURBS.

The corresponding implementation in **PySCo** is as follows
```python
# IMPORTS -------------------------------------------------------------
import numpy
import pysco.geometry.base
# ---------------------------------------------------------------------

# GEOMETRY ------------------------------------------------------------
geo = pysco.geometry.base.Geometry(dim = 2)

L = 1.0
R = 0.25

c = numpy.cos(numpy.pi/4.0)
l = (L*numpy.sqrt(2.0)/2.0-R)*c
h = R*numpy.sqrt(2.0)
geo.add_point(ID =  0, coo = (    0.0,     0.0))
geo.add_point(ID =  1, coo = (  0.5*L,     0.0))
geo.add_point(ID =  2, coo = (      L,     0.0))
geo.add_point(ID =  3, coo = (      L,   0.5*L))
geo.add_point(ID =  4, coo = (      L,       L))
geo.add_point(ID =  5, coo = (  0.5*L,       L))
geo.add_point(ID =  6, coo = (    0.0,       L))
geo.add_point(ID =  7, coo = (    0.0,   0.5*L))
geo.add_point(ID =  8, coo = (    0.0,     0.0))
geo.add_point(ID =  9, coo = (      l,       l))
geo.add_point(ID = 10, coo = (  0.5*L, 0.5*L-h))
geo.add_point(ID = 11, coo = (    L-l,       l))
geo.add_point(ID = 12, coo = (0.5*L+h,   0.5*L))
geo.add_point(ID = 13, coo = (    L-l,     L-l))
geo.add_point(ID = 14, coo = (  0.5*L, 0.5*L+h))
geo.add_point(ID = 15, coo = (      l,     L-l))
geo.add_point(ID = 16, coo = (0.5*L-h,   0.5*L))
geo.add_point(ID = 17, coo = (      l,       l))

knots_u = [0.0, 0.0, 0.0, 0.25, 0.25, 0.5, 0.5, 0.75, 0.75, 1.0, 1.0, 1.0]
knots_v = [0.0, 0.0, 1.0, 1.0]
p_u = 2
p_v = 1
conn = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
w45 = numpy.sqrt(2.0)/2.0
weights = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, w45, 1.0, w45, 1.0, w45, 1.0, w45, 1.0]

geo.add_surface(ID = "ExplicitSurfaceGeo", representation = "nurbs", knots = (knots_u, knots_v), degree = (p_u, p_v), conn = conn, weights = weights)
# ---------------------------------------------------------------------
```

Note that, without further specifications, the geometry would be _open_ with two coincident sides.
The definition of a _closed_ geometry is ensured via a periodic mesh; see LINK.

### Implicit representation
The geometry $\mathscr{D}$ can also be represented implicitly upon setting $\boldsymbol{x} = u\boldsymbol{e}_1+v\boldsymbol{e}_2$, with $(u,v) \in [0,L]\times[0,L] \equiv \mathscr{R}$, and introducing a level set function $\varphi$ such that
$$
\mathscr{D} \equiv \{\boldsymbol{x}\in\mathscr{R}~:~\varphi(\boldsymbol{x}) < 0\},
$$
where $\mathscr{R}$ denotes the backgroud square without the hole.
An example of a such a level set function is shown in figure (c) and has the following expression
$$
\varphi(\boldsymbol{x}) \equiv R^2-(x-L/2)^2-(y-L/2)^2.
$$

The corresponding implementation in **PySCo** is as follows
```python
# IMPORTS -------------------------------------------------------------
import numpy
import pysco.geometry.base
# ---------------------------------------------------------------------

# GEOMETRY ------------------------------------------------------------
geo = pysco.geometry.base.Geometry(dim = 2)

L = 1.0
R = 0.25

geo.add_surface(ID = "ImplicitSurfaceGeo", representation = "parametric", expression = ("u", "v"), domain = ([0.0, L], [0.0, L]))

geo.add_level_set_function(ID = "phi", dim = 2, type = "user_defined_expression_xyz", expression = "R*R-(x-L/2)*(x-L/2)-(y-L/2)*(y-L/2)", parameters = {"R": R, "L": L})
# ---------------------------------------------------------------------
```

Note that in the code above, the level set function is a global level set function given in terms of the global spatial coordinates $(x, y)$.
**PySCo** also allows defining local level set functions in terms of the local parametric coordinates $(u,v)$.

## Notes
[^1]: All images are generated using [ParaView](https://www.paraview.org/).
[^2]: Piegl, L., & Tiller, W. (2012). _The NURBS book_. Springer Science & Business Media. [doi](https://doi.org/10.1007/978-3-642-97385-7)