# PySCo

**PySCo** features three main modules, namely: the [Geometry Generation Module](https://gitlab.com/aeropa/pysco/-/blob/main/Docs/1-Geometry.md?ref_type=heads), the [Meshing Tools Module](https://gitlab.com/aeropa/pysco/-/blob/main/Docs/2-Mesh.md?ref_type=heads) and the [Solvers Module](https://gitlab.com/aeropa/pysco/-/blob/main/Docs/3-Solvers.md?ref_type=heads)

## Installation
Routines may be provided upon request contacting [Vincenzo Gulizzi](mailto:vincenzo.gulizzi@unipa.it).