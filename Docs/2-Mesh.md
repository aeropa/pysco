# PySCo: Meshing Tools Module

**PySCo** supports (adaptive) structured grids with embedded boundaries, polygonal and polyhedral meshes and triangular and tetrahedral meshes.
It also provides an interface with the meshing software library [Gmsh](https://gmsh.info/).

A few examples of **PySCo** meshing capabilities are given below.

## Example 1: The structured mesh of a rectangle
![alt text](./1-Geometry/1-1-Geometry.jpeg) ![alt text](./2-Mesh/2-1-Mesh.jpeg)

Consider a rectangular domain of length $L$ and height $H$ as sketched in the left figure[^1] above; it is the same geometry introduced for [Example #1](https://gitlab.com/aeropa/pysco/-/blob/main/Docs/1-Geometry.md?ref_type=heads#example-1-a-rectangle) in the documentation about the [Geometry Generation Module](https://gitlab.com/aeropa/pysco/-/blob/main/Docs/1-Geometry.md?ref_type=heads).

The right figure above shows a $8\times 4$ structured mesh, where $\mathscr{D}^e$ is a generic mesh element and $\boldsymbol{n}^e$ denotes the element's unit normal vector.

The code below shows how to build such a mesh in **PySCo**:
```python
# IMPORTS -------------------------------------------------------------
import pysco.geometry.base
import pysco.mesh.base
# ---------------------------------------------------------------------

# GEOMETRY ------------------------------------------------------------
geo = pysco.geometry.base.Geometry(dim = 2)

L = 1.0
H = 0.5

geo.add_parametric_surface(ID = "RectangleGeo", expression = ("u", "v"), domain = ([0.0, L], [0.0, H]))
# ---------------------------------------------------------------------

# MESH ----------------------------------------------------------------
msh = pysco.mesh.base.Mesh(parent_geometry = geo)

msh.init_surface_mesh(ID = "RectangleMsh", parent_ID = "RectangleGeo")

p = 3
q = 5
msh.set_surface_mesh(ID = "RectangleMsh",                 # ID of the mesh object to be meshed
                     subdivision = "structured",          # Mesh type
                     n_cells = (8, 4),                    # Number of cells
                     basis_functions_space = "LegendreP", # Use Legendre polynomials as basis functions
                     basis_functions_order = p,           # Use Legendre polynomials up to order p
                     quadrature_order = q)                # Use q-th order quadrature rules

# GENERATE THE MESH
msh.generate()
# ---------------------------------------------------------------------
```

## Example 2: Multiple meshing strategies
![alt text](./2-Mesh/2-2-MultipleMeshingStrategies.jpeg)

Consider now the [Example #3](https://gitlab.com/aeropa/pysco/-/blob/main/Docs/1-Geometry.md?ref_type=heads#example-3-two-equivalent-squares-with-a-circular-hole) in the documentation about the [Geometry Generation Module](https://gitlab.com/aeropa/pysco/-/blob/main/Docs/1-Geometry.md?ref_type=heads).
The geometry consisted of a square of size $L$ with a central circular hole of radius $R$.
**PySCo** allows partitioning this geometry through different meshing strategies, which consist of using 
(a) an unstructured mesh (thanks to **PySCo** interface with [Gmsh](https://gmsh.info/)),
(b) a structured mesh,
(c) an embedded-boundary mesh,
(d) a polygonal mesh (thanks to **PySCo** interface with [VORO++](https://math.lbl.gov/voro++/) or [Neper](https://neper.info/)).

### Unstructured Mesh
To use an unstructured mesh **PySCo** interfaces with the [Gmsh](https://gmsh.info/) library and must construct the geometry accordingly.

Below the implementation in **PySCo**
```python
# IMPORTS -------------------------------------------------------------
import numpy
import pysco.geometry.base
# ---------------------------------------------------------------------

# GEOMETRY ------------------------------------------------------------
geo = pysco.geometry.base.Geometry(dim = 2)

L = 1.0
R = 0.25
xc, yc = 0.5*L, 0.5*L

geo.add_point(ID = 0, coo = (0.0, 0.0))
geo.add_point(ID = 1, coo = (  L, 0.0))
geo.add_point(ID = 2, coo = (  L,   L))
geo.add_point(ID = 3, coo = (0.0,   L))

geo.add_point(ID = 4, coo = (  xc,   yc))
geo.add_point(ID = 5, coo = (xc-R,   yc))
geo.add_point(ID = 6, coo = (  xc, yc+R))
geo.add_point(ID = 7, coo = (xc+R,   yc))
geo.add_point(ID = 8, coo = (  xc, yc-R))

geo.add_curve(ID = 0, representation = "line", conn = (0, 1))
geo.add_curve(ID = 1, representation = "line", conn = (1, 2))
geo.add_curve(ID = 2, representation = "line", conn = (2, 3))
geo.add_curve(ID = 3, representation = "line", conn = (3, 0))

geo.add_curve(ID = 4, representation = "circle_arc", conn = (5, 4, 6))
geo.add_curve(ID = 5, representation = "circle_arc", conn = (6, 4, 7))
geo.add_curve(ID = 6, representation = "circle_arc", conn = (7, 4, 8))
geo.add_curve(ID = 7, representation = "circle_arc", conn = (8, 4, 5))

geo.add_curve_loop(ID = 0, conn = (0, 1, 2, 3), sign = (+1, +1, +1, +1))
geo.add_curve_loop(ID = 1, conn = (4, 5, 6, 7), sign = (+1, +1, +1, +1))

geo.add_surface(ID = "SurfaceGeo", representation = "from_curve_loops", conn = (0, 1), sign = (+1, +1))
# ---------------------------------------------------------------------

# MESH ----------------------------------------------------------------
msh = pysco.mesh.base.Mesh(parent_geometry = geo)

msh.init_surface_mesh(ID = "SurfaceMsh", parent_ID = "SurfaceGeo")

p = 3
q = 5
msh.set_surface_mesh(ID = "SurfaceMsh",
                     subdivision = "tri",
                     basis_functions_space = "LegendreP",
                     basis_functions_order = p,
                     quadrature_order = q)

# GENERATE THE MESH
h = 0.1
msh.generate(library = "gmsh", mesh_size = h)
# ---------------------------------------------------------------------
```

### Structured Mesh

### Embedded-boundary Mesh

### Polygonal Mesh

## Notes
[^1]: All images are generated using [ParaView](https://www.paraview.org/).
